#include <cassert>
#include <iostream>
#include <type_traits>
#include <utility>
#include <vector>
#include <iterator>

#include "ll.h"
#include "mallocator.h"
#include <forward_list>

#include <cxxabi.h>

void test_default_constructor()
{
  std::cout << "test default constructor\n";
  ll::forward_list<int> list;
  assert(list.empty());
}

void test_count_constructor()
{
  std::cout << "test count constructor\n";
  ll::forward_list<int> list(10, -1);
  assert(list.size() == 10);
  for (const auto &i : list)
    assert(i == -1);

  ll::forward_list<int> list2(10);
  assert(list.size() == 10);
  for (const auto &i : list2) {
    assert(i == 0);
  }
}

void test_iterator_constructor()
{
  std::cout << "test iterator constructor\n";
  std::vector<int> v = { 9, 8, 7, 6, 5, 4, 3, 2, 1, 0 };
  ll::forward_list<int> list(v.begin(), v.end());
  assert(list.size() == 10);

  auto p = list.begin();
  for (int i=0; i<10; i++) {
    assert(*p == v[i]);
    p++;
  }


  ll::forward_list<int> list2({1, 2, 3, 4, 5});
  assert(list2.size() == 5);
  auto p2 = list2.begin();
  for (int i=1; i<=5; i++) {
    assert(*p2 == i);
    p2++;
  }
}


template <typename Tag>
void print_it_category()
{
  int r;
  char *s = abi::__cxa_demangle(typeid(Tag).name(), nullptr, nullptr, &r);
  std::cout << "tag: " << s << "\n";
  free(s);
}

template <typename T>
void print_it_category(const T &)
{
  print_it_category<typename std::iterator_traits<T>::iterator_category>();
}

void test_copy_constructor()
{
  std::cout << "test copy constructor\n";
  ll::forward_list<int> list({1, 2, 3, 4, 5});
  ll::forward_list<int> list2 = list;
  assert(list2.size() == 5);

  auto p1 = list.begin();
  auto p2 = list2.begin();

  for (int i=0; i<5; i++) {
    assert(*p1 == *p2);
    p1++;
    p2++;
  }
}

void test_move_constructor()
{
  std::cout << "test move constructor\n";
  ll::forward_list<int> list({1, 2, 3, 4, 5});
  ll::forward_list<int> list2(std::move(list));
  assert(list2.size() == 5);
  assert(list.size() == 0);

  auto p2 = list2.begin();
  for (int i=1; i<=5; i++) {
    assert(*p2 == i);
    p2++;
  }
}

void test_copy_assignment()
{
  std::cout << "test copy assignment\n";
  ll::forward_list<int> l1({1, 2, 3, 4, 5});
  ll::forward_list<int> l2({5, 10, 15});
  l1 = l2;
  assert(l1.size() == l2.size());

  auto p = l2.begin();
  for (auto &i : l1) {
    assert(i == *p);
    p++;
  }
}

void test_move_assignment()
{
  std::cout << "test move assignment\n";
  ll::forward_list<int> l1({1, 2, 3, 4, 5});
  ll::forward_list<int> l2({5, 10, 15});
  l1 = std::move(l2);
  assert(l1.size() == 3);

  int vals[] = {5, 10, 15};
  int cur = 0;
  for (auto &i : l1) {
    assert(i == vals[cur]);
    cur++;
  }
}

void test_assignment_initializer_list()
{
 std::cout << "test assignment from initializer list\n";
  ll::forward_list<int> l1({1, 2, 3, 4, 5});
  l1 = {5, 10, 15};
  assert(l1.size() == 3);

  int vals[] = {5, 10, 15};
  int cur = 0;
  for (auto &i : l1) {
    assert(i == vals[cur]);
    cur++;
  }

}


void test_push_front()
{
  std::cout << "test push front\n";
  /*
  std::forward_list<int, Mallocator<int>> list;
  list.push_front(5);
  assert(list.front() == 5);
  */

  ll::forward_list<int> list;
  list.push_front(5);
  assert(list.front() == 5);
  assert(list.empty() == false);

  list.push_front(10);
  assert(list.front() == 10);
  assert(list.empty() == false);
}

void test_clear()
{
  std::cout << "test clear\n";
  ll::forward_list<int> list;
  for (int i=0; i<10; i++) {
    list.push_front(i);
    assert(list.front() == i);
    assert(list.empty() == false);
  }

  list.clear();
  assert(list.empty());
}

class A
{
public:
  A(int _i) : i(new int(_i)) { std::cout << "A created: " << *i << "\n"; }

  A(const A &rhs) : i(new int (*rhs.i))
  { std::cout << "A coppied: " << *i << "\n"; }

  A(A &&rhs) : i(rhs.i)
  {
    std::cout << "A moved: " << i << "\n";
    rhs.i = nullptr;
  }

  ~A()
  {
    if (i)  {
      std::cout << "A destroyed: " << *i << "\n";
      free(i);
    }
    else
      std::cout << "A destroyed: " << "(nil)" << "\n";
  }

  int *i;
};

std::ostream &operator<<(std::ostream &out, const A &a)
{
  if (a.i) {
    out << "A[" << *a.i << "]";
  }
  else {
    out << "A[(nil)]";
  }

  return out;
}

void test_verify_destructors_called()
{
  std::cout << "test verify destructors called\n";

  ll::forward_list<A> list;
  list.push_front(A(3));
  list.clear();

  A a1(1);
  A a2(2);
  A a3(3);
  list.push_front(a1);
  list.push_front(a2);
  list.push_front(a3);
  list.clear();
}

template <typename InputIt,
          typename = ll::detail::RequiresInputIterator<InputIt>>
void check_input_iterator(InputIt)
{
  std::cout << "is InputIterator\n";
}


void test_const_iterators()
{
  ll::forward_list<int> list;
  for (int i=0; i<10; i++)
    list.push_front(i+1);

  for (const auto &i: list) {
    std::cout << i << " ";
  }
  std::cout << "\n";

  auto cp = list.cbegin();
//  *cp = 5;
  while (cp != list.cend()) {
    std::cout << *cp << " ";
    cp++;
  }
  std::cout << "\n";
  list.clear();

  check_input_iterator(list.cbegin());
}

void test_iterators()
{

  ll::forward_list<int> list;
  for (int i=0; i<10; i++)
    list.push_front(i+1);

  for (const auto &i: list) {
    std::cout << i << " ";
  }
  std::cout << "\n";

  for (auto &i: list) {
    i += 1;
  }

  for (const auto &i: list) {
    std::cout << i << " ";
  }
  std::cout << "\n";

  auto p = list.begin();
  *p = 5;
  while (p != list.end()) {
    *p = (*p) * 2;
    p++;
  }

  for (const auto &i: list) {
    std::cout << i << " ";
  }

  std::cout << "\n";
  list.clear();

  check_input_iterator(list.begin());
  check_input_iterator(list.before_begin());
}

class B
{
public:
  B(int i) : x(i) {}
  int x;
};

void test_iterator_member_access()
{
  ll::forward_list<B> list;
  for (int i=0; i<10; i++) {
    list.push_front(B(i));
  }

  for (auto p = list.cbegin(); p != list.cend(); ++p) {
    std::cout << p->x << " ";
  }
  std::cout << "\n";

  for (auto p = list.begin(); p != list.end(); ++p) {
    std::cout << p->x << " ";
  }
  std::cout << "\n\n";

  list.clear();
}

void test_insert_after()
{
  ll::forward_list<int> list;
  for (int i=0; i<10; i++) {
    list.push_front(i+1);
  }

  std::cout << "before: \n";
  for (const auto &i: list) {
    std::cout << i << "  ";
  }
  std::cout << "\n";

  auto p = list.begin();
  for (int i=0; i<5; i++)
    ++p;

  int next = 100;
  std::cout << "inserting " << next << " after " << *p << "\n";
  auto n = list.insert_after(p, next);

  assert(*p == 5);
  assert(*n == 100);
  for (const auto &i : list)
    std::cout << i << "  ";
  std::cout << "\n\n";

  list.clear();

  for (int i=0; i<10; i++) {
    list.push_front(i+1);
  }

  std::cout << "before: \n";
  for (const auto &i: list) {
    std::cout << i << "  ";
  }
  std::cout << "\n";

  p = list.begin();
  for (int i=0; i<5; i++)
    ++p;

  std::cout << "inserting 500 after " << *p << "\n";
  n = list.insert_after(p, 500);
  assert(*p == 5);
  assert(*n == 500);
  for (const auto &i : list)
    std::cout << i << " ";
  std::cout << "\n\n";

  list.clear();

  for (int i=0; i<10; i++) {
    list.push_front(i+1);
  }

  std::cout << "before: \n";
  for (const auto &i: list) {
    std::cout << i << "  ";
  }
  std::cout << "\n";

  p = list.begin();
  for (int i=0; i<9; i++)
    ++p;

  std::cout << "inserting 500 after " << *p << "\n";
  n = list.insert_after(p, 500);
  assert(*n == 500);
  for (const auto &i : list)
    std::cout << i << " ";
  std::cout << "\n\n";

  list.clear();

  for (int i=0; i<10; i++) {
    list.push_front(i+1);
  }

  std::cout << "before: \n";
  for (const auto &i: list) {
    std::cout << i << "  ";
  }
  std::cout << "\n";

  p = list.begin();
  std::cout << "inserting 500 after " << *p << "\n";
  n = list.insert_after(p, 500);
  assert(*n == 500);
  for (const auto &i : list)
    std::cout << i << " ";
  std::cout << "\n\n";


  list.clear();

  for (int i=0; i<10; i++) {
    list.push_front(i+1);
  }

  std::cout << "before: \n";
  for (const auto &i: list) {
    std::cout << i << "  ";
  }
  std::cout << "\n";

  assert(list.begin() != list.before_begin());

  auto q = list.before_begin();
  std::cout << "inserting 500 after before_begin()\n";
  n = list.insert_after(q, 500);
  assert(*n == 500);
  for (const auto &i : list)
    std::cout << i << " ";
  std::cout << "\n\n";
}

void test_insert_after_count()
{
  ll::forward_list<int> list;
  for (int i=0; i<10; i++) {
    list.push_front(i+1);
  }

  std::cout << "before: \n";
  for (const auto &i: list) {
    std::cout << i << "  ";
  }
  std::cout << "\n";

  auto p = list.begin();
  for (int i=0; i<5; i++)
    ++p;

  assert(list.size() == 10);
  int next = 100;
  std::cout << "inserting 5 " << next << " after " << *p << "\n";
  auto n = list.insert_after(p, 5u, next);

  assert(*p == 5);
  assert(*++p == 100);
  assert(*n == 100);

  for (const auto &i : list)
    std::cout << i << "  ";
  std::cout << "\n\n";

  list.clear();
  for (int i=0; i<10; i++) {
    list.push_front(i+1);
  }

  std::cout << "before: \n";
  for (const auto &i: list) {
    std::cout << i << "  ";
  }
  std::cout << "\n";

  p = list.begin();
  for (int i=0; i<5; i++)
    ++p;

  assert(list.size() == 10);
  next = 100;
  std::cout << "inserting 0 " << next << " after " << *p << "\n";
  n = list.insert_after(p, 0, next);

  assert(n == p);

  for (const auto &i : list)
    std::cout << i << "  ";
  std::cout << "\n\n";

  list.clear();
  for (int i=0; i<10; i++) {
    list.push_front(i+1);
  }

  std::cout << "before: \n";
  for (const auto &i: list) {
    std::cout << i << "  ";
  }
  std::cout << "\n";

  p = list.before_begin();
  assert(list.size() == 10);
  next = 100;
  std::cout << "inserting 3 " << next << " after " << *p << "\n";
  n = list.insert_after(p, 3, next);
  assert(list.size() == 13);
  assert(*n == 100);

  for (const auto &i : list)
    std::cout << i << "  ";
  std::cout << "\n\n";
}

void test_insert_after_iter()
{
  ll::forward_list<int> list;
  for (int i=0; i<10; i++) {
    list.push_front(i+1);
  }

  std::cout << "before: \n";
  for (const auto &i: list) {
    std::cout << i << "  ";
  }
  std::cout << "\n";

  auto p = list.begin();
  for (int i=0; i<5; i++)
    ++p;

  std::vector<int> v;
  for (int i=0; i<5; i++)
    v.push_back(i+5);

  assert(list.size() == 10);
  std::cout << "v: ";
  for (auto &i : v) {
    std::cout << i << "  ";
  }
  std::cout << "\n";
  std::cout << "inserting v after " << *p << "\n";
  auto n = list.insert_after(p, v.begin(), v.end());
  assert(*n == v[v.size() - 1]);

  for (const auto &i : list)
    std::cout << i << "  ";
  std::cout << "\n\n";

  list.clear();
  for (int i=0; i<10; i++) {
    list.push_front(i+1);
  }

  std::cout << "before: \n";
  for (const auto &i: list) {
    std::cout << i << "  ";
  }
  std::cout << "\n";

  p = list.begin();
  for (int i=0; i<5; i++)
    ++p;

  assert(list.size() == 10);
  std::cout << "v: ";
  for (auto &i : v) {
    std::cout << i << "  ";
  }
  std::cout << "\n";
  std::cout << "inserting (v.begin(), v.begin()) after " << *p << "\n";
  n = list.insert_after(p, v.begin(), v.begin());
  assert(n == p);

  for (const auto &i : list)
    std::cout << i << "  ";
  std::cout << "\n\n";
}

void test_insert_after_ilist()
{
  ll::forward_list<int> list;
  for (int i=0; i<10; i++) {
    list.push_front(i+1);
  }

  std::cout << "before: \n";
  for (const auto &i: list) {
    std::cout << i << "  ";
  }
  std::cout << "\n";

  auto p = list.begin();
  for (int i=0; i<5; i++)
    ++p;

  assert(list.size() == 10);
  std::cout << "inserting {1, 2, 3} after " << *p << "\n";
  auto n = list.insert_after(p, {1, 2, 3});
  assert(*n = 3);
  assert(list.size() == 13);

  for (const auto &i : list)
    std::cout << i << "  ";
  std::cout << "\n\n";

  list.clear();
  for (int i=0; i<10; i++) {
    list.push_front(i+1);
  }

  std::cout << "before: \n";
  for (const auto &i: list) {
    std::cout << i << "  ";
  }
  std::cout << "\n";

  p = list.begin();
  for (int i=0; i<5; i++)
    ++p;

  assert(list.size() == 10);
  std::cout << "inserting {} after " << *p << "\n";
  n = list.insert_after(p, {});
  assert(n == p);
  assert(list.size() == 10);

  for (const auto &i : list)
    std::cout << i << "  ";
  std::cout << "\n\n";


}



class C
{
public:
  C(int x, int y, int z) : a(x), b(y), c(z){}
  int a, b, c;
};

std::ostream & operator<<(std::ostream &out, const C &c)
{
  out << "C[" << c.a << ", " << c.b << ", " << c.c << "]";
  return out;
}

void test_emplace_after()
{
  ll::forward_list<C> list;
  for (int i=0; i<10; i++) {
    list.push_front(C(i, i+1, i+2));
  }

  for (auto &t : list)
    std::cout << t << "\n";
  std::cout <<"\n";


  auto p = list.begin();
  for (int i=0; i<5; i++) {
    ++p;
  }

  std::cout << "inserting after " << *p << "\n";
  auto n = list.emplace_after(p, 100, 200, 300);

  assert(n->a == 100);
  assert(n->b == 200);
  assert(n->c == 300);
  std::cout << "\n\n";

  for (auto &t : list) {
    std::cout << t << "\n";
  }
  std::cout << "\n\n";
}

void test_erase_after()
{
  ll::forward_list<int> list;
  for (int i=0; i<10; i++) {
    list.push_front(i+1);
  }

  std::cout << "before: \n";
  for (auto &t : list)
    std::cout << t << "  ";
  std::cout <<"\n";

  auto p = list.begin();
  for (int i=0; i<5; i++) {
    ++p;
  }

  std::cout << "erasing after " << *p << "\n";
  auto n = list.erase_after(p);

  ++p;

  assert(*p == 3);
  assert(n == p);

  for (auto &t : list) {
    std::cout << t << "  ";
  }
  std::cout << "\n\n";


  list.clear();

  for (int i=0; i<10; i++) {
    list.push_front(i+1);
  }

  std::cout << "before: \n";
  for (auto &t : list)
    std::cout << t << "  ";
  std::cout <<"\n";


  p = list.begin();
  for (int i=0; i<8; i++) {
    ++p;
  }

  std::cout << "erasing after " << *p << "\n";
  n = list.erase_after(p);
  ++p;

  assert(p == list.end());
  assert(n == p);

  for (auto &t : list) {
    std::cout << t << "  ";
  }
  std::cout << "\n\n";


  list.clear();

  for (int i=0; i<10; i++) {
    list.push_front(i+1);
  }

  std::cout << "before: \n";
  for (auto &t : list)
    std::cout << t << "  ";
  std::cout <<"\n";


  p = list.begin();
  for (int i=0; i<9; i++) {
    ++p;
  }

  std::cout << "erasing after " << *p << "\n";
  n = list.erase_after(p);
  ++p;

  assert(p == list.end());
  assert(n == p);

  for (auto &t : list) {
    std::cout << t << "  ";
  }
  std::cout << "\n\n";

  list.clear();

  for (int i=0; i<10; i++) {
    list.push_front(i+1);
  }

  std::cout << "before: \n";
  for (auto &t : list)
    std::cout << t << "  ";
  std::cout <<"\n";


  p = list.before_begin();
  std::cout << "erasing after before_begin()\n";
  n = list.erase_after(p);
  ++p;

  assert(n == p);

  for (auto &t : list) {
    std::cout << t << "  ";
  }
  std::cout << "\n\n";

}

void test_erase_after_range()
{
  ll::forward_list<int> list;
  for (int i=0; i<10; i++)
    list.emplace_front(i+1);

  assert(list.size() == 10u);

  std::cout << "before: \n";
  for (const auto &i: list) {
    std::cout << i << "  ";
  }
  std::cout << "\n";




  auto p = list.begin();
  for (int i=0; i<3; i++)
    ++p;
  auto q = p;
  for (int i=0; i<5; i++)
    ++q;

  std::cout << "erase_after (" << *p << ", " << *q << ")\n";
  auto a = list.erase_after(p, q);
  assert(list.size() == 6u);

  for (const auto &i: list) {
    std::cout << i << "  ";
  }
  std::cout << "\n";
  std::cout << "ret: " << *a << "\n";
  std::cout << "\n\n";


  list.clear();
  for (int i=0; i<10; i++)
    list.emplace_front(i+1);
  assert(list.size() == 10);

  std::cout << "before: \n";
  for (const auto &i: list) {
    std::cout << i << "  ";
  }
  std::cout << "\n";

  p = list.begin();
  for (int i=0; i<3; i++)
    ++p;
  q = list.end();

  std::cout << "erase_after (" << *p << ", list.end())\n";
  a = list.erase_after(p, q);
  assert(list.size() == 4);

  for (const auto &i: list) {
    std::cout << i << "  ";
  }
  std::cout << "\n";
  if (a == list.end())
    std::cout << "ret: list.end()\n";
  else
    std::cout << "ret: " << *a << "\n";

  std::cout << "\n\n";


#if 0
  list.clear();
  for (int i=0; i<10; i++)
    list.emplace_front(i+1);

  std::cout << "before: \n";
  for (const auto &i: list) {
    std::cout << i << "  ";
  }
  std::cout << "\n";

  p = list.begin();
  for (int i=0; i<3; i++)
    ++p;
  q = p;
  for (int i=0; i<5; i++)
    ++q;

  std::cout << "erase_after (" << *q << ", " << *p << ")\n";
  a = list.erase_after(q, p);

  for (const auto &i: list) {
    std::cout << i << "  ";
  }
  std::cout << "\n";
  std::cout << "ret: " << *a << "\n";
  std::cout << "\n\n";
#endif

}


void test_emplace_front()
{
  ll::forward_list<C> list;
  for (int i=0; i<10; i++) {
    list.emplace_front(i+1, i+2, i+3);
  }

  int i = 9;
  for (const auto &c: list) {
    assert(c.a == i+1 && c.b == i+2 && c.c == i+3);
    std::cout << i << ". " << c << "\n";
    i--;
  }
  std::cout << "\n\n";
}

void test_pop_front()
{
  ll::forward_list<int> list;
  for (int i=0; i<10; i++) {
    list.emplace_front(i);
  }

  int i = 9;
  while (!list.empty())
  {
    assert(list.front() == i);
    list.pop_front();
    i--;
  }
}

void test_resize()
{
  ll::forward_list<int> list;

  for (int i=0; i<5; i++)
    list.emplace_front(i+1);
  assert(list.size() == 5);

  std::cout << "before: ";
  for (auto &i : list)
    std::cout << i << "  ";
  std::cout << "\n";

  std::cout << "resize(10)\n";
  list.resize(10);
  assert(list.size() == 10);

  for (auto &i : list)
    std::cout << i << "  ";
  std::cout << "\n\n";

  list.clear();
  for (int i=0; i<10; i++)
    list.emplace_front(i+1);
  assert(list.size() == 10);

  std::cout << "before: ";
  for (auto &i : list)
    std::cout << i << "  ";
  std::cout << "\n";

  std::cout << "resize(5)\n";
  list.resize(5);
  assert(list.size() == 5);

  for (auto &i : list)
    std::cout << i << "  ";
  std::cout << "\n\n";

  list.clear();
  for (int i=0; i<10; i++)
    list.emplace_front(i+1);
  assert(list.size() == 10);

  std::cout << "before: ";
  for (auto &i : list)
    std::cout << i << "  ";
  std::cout << "\n";

  std::cout << "resize(15)\n";
  list.resize(15, -1);
  assert(list.size() == 15);

  for (auto &i : list)
    std::cout << i << "  ";
  std::cout << "\n\n";

  list.clear();
  for (int i=0; i<10; i++)
    list.emplace_front(i+1);
  assert(list.size() == 10);

  std::cout << "before: ";
  for (auto &i : list)
    std::cout << i << "  ";
  std::cout << "\n";

  std::cout << "resize(0)\n";
  list.resize(0, -1);
  assert(list.size() == 0);

  for (auto &i : list)
    std::cout << i << "  ";
  std::cout << "\n\n";
}

void test_swap()
{
  ll::forward_list<int> l1, l2;
  for (int i=0; i<10; i++) {
    l1.emplace_front(i + 'A');
  }

  for (int i=0; i<5; i++) {
    l2.emplace_front(i + 'Q');
  }

  std::cout << "l1: ";
  for(const auto &i : l1)
    std::cout << char(i) << " ";
  std::cout << "\n";

  std::cout << "l2: ";
  for(const auto &i : l2)
    std::cout << char(i) << " ";
  std::cout << "\n";


  assert(l1.size() == 10u);
  assert(l2.size() == 5u);
  using std::swap;

  swap(l1, l2);
  assert(l1.size() == 5u);
  assert(l2.size() == 10u);

  std::cout << "l1: ";
  for(const auto &i : l1)
    std::cout << char(i) << " ";
  std::cout << "\n";

  std::cout << "l2: ";
  for(const auto &i : l2)
    std::cout << char(i) << " ";
  std::cout << "\n";
}


int main()
{
  test_default_constructor();
  test_count_constructor();
  test_iterator_constructor();
  test_copy_constructor();
  test_move_constructor();

  test_copy_assignment();
  test_move_assignment();
  test_assignment_initializer_list();

  test_push_front();
  test_clear();
  test_verify_destructors_called();
  test_const_iterators();
  test_iterators();
  test_iterator_member_access();

  test_insert_after();
  test_insert_after_count();
  test_insert_after_iter();
  test_insert_after_ilist();
  test_emplace_after();
  test_erase_after();
  test_erase_after_range();
  test_emplace_front();
  test_pop_front();
  test_resize();
  test_swap();

  printf("all tests pass\n");
}
