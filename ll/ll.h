#ifndef LL_H
#define LL_H

#include <memory>
#include <new>
#include <cstdint>
#include <iterator>
#include <type_traits>

// \todo: this business with allocators is half-assed.  need to figure out
//        how they should be done.
//
// \todo: insert_after and constructors should have strong exception guarantee

namespace ll {

namespace detail {

template <typename Iterator>
using RequiresInputIterator =
  typename std::enable_if_t<
    std::is_convertible<
      typename std::iterator_traits<Iterator>::iterator_category,
      std::input_iterator_tag
    >::value>;

template <typename Allocator>
inline static
auto copy_allocator(const Allocator &alloc) {
  using traits = std::allocator_traits<Allocator>;
  return traits::select_on_container_copy_construction(alloc);
}

struct node_base
{
  struct node_base *next;
  node_base() noexcept : next(nullptr) {}
};

template <class T>
struct node : node_base
{
  typename std::aligned_storage<sizeof(T), alignof(T)>::type space;

  node() noexcept = default;
  ~node() = default;

  T *ptr() noexcept { return reinterpret_cast<T *>(&space); }
  const T* ptr() const noexcept { return ptr(); }

  explicit node(const T &t){ new (&space) T(t); }
  explicit node(T &&t){ new (&space) T(std::move(t)); }

  template <typename ...Args>
  explicit node(Args&&...args)
  { new (&space) T(std::forward<Args>(args)...); }
};

template <class T>
class iterator
{
public:
  using iterator_category = std::forward_iterator_tag;
  using difference_type   = std::ptrdiff_t;
  using value_type        = T;
  using pointer           = T*;
  using reference         = T&;
  using const_pointer     = const T*;
  using const_reference   = const T&;
  using node_t            = node<T>;

  iterator() noexcept = default;
  explicit iterator(node_base *n) noexcept : p(n) {}

  bool operator==(const iterator &other) const noexcept
  { return p == other.p; }
  bool operator!=(const iterator &other) const noexcept
  { return !(*this == other); }

  iterator &operator++() noexcept
  {
    p = p->next;
    return *this;
  }

  iterator operator++(int) noexcept
  {
    auto tmp = iterator(p);
    p = p->next;
    return tmp;
  }

  reference operator*() noexcept
  { return *static_cast<node_t *>(p)->ptr(); }

  const_reference operator*() const noexcept
  { return *static_cast<node_t *>(p)->ptr(); }

  pointer operator->() noexcept
  { return static_cast<node_t *>(p)->ptr(); }

  const_pointer operator->() const noexcept
  { return &static_cast<node_t *>(p)->ptr(); }

  node_base *get() noexcept { return p; }
  const node_base *get() const noexcept { return p; }

private:
  node_base *p;
};

template <typename T>
class const_iterator
{
public:
  using iterator_category = std::forward_iterator_tag;
  using difference_type   = std::ptrdiff_t;
  using value_type        = T;
  using pointer           = const T*;
  using reference         = const T&;
  using node_t            = node<T>;

  const_iterator() noexcept : p(nullptr) {}
  explicit const_iterator(const node_base *n) noexcept :
    p(const_cast<node_base *>(n)) {}
  const_iterator(const iterator<T> &i) noexcept :
    p(const_cast<node_base *>(i.get())) {}

  bool operator==(const const_iterator &other) const noexcept
  { return p == other.p; }
  bool operator!=(const const_iterator &other) const noexcept
  { return !(*this == other); }

  const_iterator &operator++() noexcept
  {
    p = p->next;
    return *this;
  }

  const_iterator operator++(int) noexcept
  {
    auto tmp = const_iterator(p);
    p = p->next;
    return tmp;
  }

  reference operator*() const noexcept
  { return *static_cast<node_t *>(p)->ptr(); }

  pointer operator->() const noexcept
  { return static_cast<node_t *>(p)->ptr(); }

  node_base *get() noexcept { return p; }
  const node_base *get() const noexcept { return p; }

private:
  node_base *p;
};

} //namespace detail


template <class T, class Allocator = std::allocator<T>>
class forward_list
{
public:
  using allocator_type  = Allocator;
  using traits          = typename std::allocator_traits<Allocator>;
  using pointer         = typename traits::pointer;
  using const_pointer   = typename traits::const_pointer;

  using difference_type = std::ptrdiff_t;
  using size_type       = std::size_t;
  using value_type      = T;
  using reference       = T&;
  using const_reference = const T&;
  using node_type       = detail::node<T>;
  using iterator        = detail::iterator<T>;
  using const_iterator  = detail::const_iterator<T>;

public:

  // member functions
  // constructors

  explicit forward_list(const Allocator &allocator) :
    alloc(allocator), root(), count(0)
  {}

  forward_list() : forward_list(Allocator()) {}

  forward_list(size_type count, const T &value,
               const Allocator &allocator = Allocator()) :
    forward_list(allocator)
  {
    insert_after(cbefore_begin(), count, value);
  }

  explicit forward_list(size_type count,
                        const Allocator &allocator = Allocator()) :
    forward_list(count, T(), allocator)
  {}

  template <class InputIt,
            typename = detail::RequiresInputIterator<InputIt>>
  forward_list(InputIt first, InputIt last,
               const Allocator &allocator = Allocator()) :
    forward_list(allocator)
  {
    insert_after(cbefore_begin(), first, last);
  }

  forward_list(const forward_list &other, const Allocator &alloc) :
    forward_list(other.begin(), other.end(), alloc)
  {}

  forward_list(const forward_list &other) :
    forward_list(other.begin(), other.end(),
                 detail::copy_allocator(other.get_allocator()))
  {}

  forward_list(forward_list &&other) :
    alloc(std::move(other.alloc))
  {
    count = other.count;
    root.next = other.root.next;

    other.count = 0;
    other.root.next = nullptr;
  }

  forward_list(forward_list &&other, const Allocator &allocator) :
    alloc(allocator)
  {
    if (std::is_same<decltype(allocator),
                     decltype(other.get_allocator())>::value) {
      count = other.count;
      root.next = other.root.next;

      other.count = 0;
      other.root.next = nullptr;
    }
    else {
      iterator i = cbefore_begin();
      while(!other.empty()){
        i = insert_after(i, std::move(other.front()));
        other.pop_front();
      }
    }
  }

  forward_list(std::initializer_list<T> init,
               const Allocator &alloc = Allocator()) :
    forward_list(init.begin(), init.end(), alloc)
  {}

  forward_list &operator=(const forward_list &other)
  {
    clear();
    insert_after(cbefore_begin(), other.begin(), other.end());
    return *this;
  }

  forward_list &operator=(forward_list &&other)
  {
    swap(other);
    return *this;
  }

  forward_list &operator=(std::initializer_list<T> ilist)
  {
    clear();
    insert_after(cbefore_begin(), ilist.begin(), ilist.end());
    return *this;
  }

  ~forward_list() noexcept { clear(); }
  allocator_type get_allocator() const { return alloc; }

  //element access
  reference front() { return *begin(); }
  const_reference front() const { return *cbegin(); }

  // iterators
  iterator begin() noexcept { return iterator(root.next); }
  const_iterator begin() const noexcept { return const_iterator(root.next); }
  const_iterator cbegin() const noexcept  {return const_iterator(root.next); }

  iterator end() noexcept { return iterator(nullptr); }
  const_iterator end() const noexcept {return const_iterator(nullptr); }
  const_iterator cend() const noexcept {return const_iterator(nullptr); }

  iterator before_begin() noexcept
  { return iterator(&root); }
  const_iterator before_begin() const noexcept
  { return const_iterator(&root); }
  const_iterator cbefore_begin() const noexcept
  { return const_iterator(&root); }

  // capacity
  bool empty() const noexcept { return count == 0; }
  size_type max_size() const noexcept { return SIZE_MAX; }

  // modifiers
  void clear() noexcept
  {
    erase_after(cbefore_begin(), cend());
  }

  iterator insert_after(const_iterator pos, const T &value)
  {
    auto n = create_node(value);
    return insert_after(pos, n);
  }

  iterator insert_after(const_iterator pos, T &&value)
  {
    auto n = create_node(std::move(value));
    return insert_after(pos, n);
  }

  iterator insert_after(const_iterator pos, size_type count, const T &value)
  {
    iterator i(pos.get());
    while (count) {
      auto n = create_node(value);
      i = insert_after(i, n);
      --count;
    }
    return i;
  }

  // stole the Requires() from the libstdc++ header
  template <typename InputIt,
            typename = detail::RequiresInputIterator<InputIt>>
  iterator insert_after(const_iterator pos, InputIt first, InputIt last)
  {
    iterator i(pos.get());
    while (first != last) {
      auto n = create_node(*first);
      i = insert_after(i, n);
      ++first;
    }

    return i;
  }

  iterator insert_after(const_iterator pos, std::initializer_list<T> ilist)
  {
    return insert_after(pos, ilist.begin(), ilist.end());
  }

  template <class... Args>
  iterator emplace_after(const_iterator pos, Args&& ... args)
  {
    auto n = create_node(std::forward<Args>(args)...);
    return insert_after(pos, n);
  }

  iterator erase_after(const_iterator pos)
  {
    auto p = pos.get();
    auto t = static_cast<node_type *>(p->next);

    if (!t)
      return iterator(nullptr);

    p->next = t->next;
    t->~node();
    alloc.deallocate(t, sizeof(*t));
    --count;
    return iterator(p->next);
  }

  iterator erase_after(const_iterator first, const_iterator last)
  {
    auto start = first.get();
    auto to_delete = start->next;
    auto end = last.get();

    start->next = end;
    while (to_delete != end) {
      auto t = static_cast<node_type *>(to_delete);
      to_delete = to_delete->next;

      t->~node();
      alloc.deallocate(t, sizeof(*t));
      --count;
    }

    return iterator(end);
  }

  void push_front(const T &value)
  {
    auto n = create_node(value);
    insert_after(cbefore_begin(), n);
  }

  void push_front(T &&value)
  {
    auto n = create_node(std::move(value));
    insert_after(cbefore_begin(), n);
  }

  template <typename ...Args>
  void emplace_front(Args &&...args)
  {
    auto n = create_node(std::forward<Args>(args)...);
    insert_after(cbefore_begin(), n);
  }

  void pop_front()
  {
    erase_after(cbefore_begin());
  }

  void resize(size_type count, const value_type &value)
  {
    auto *n = &root;
    while (count && n->next) {
      n = n->next;
      count--;
    }

    if (!count) {
      erase_after(const_iterator(n), cend());
    }
    else {
      insert_after(const_iterator(n), count, value);
    }
  }

  void resize(size_type count)
  {
    resize(count, T());
  }

  void swap(forward_list &other)
  {
    using std::swap;

    swap(alloc, other.alloc);
    swap(root.next, other.root.next);
    swap(count, other.count);
  }

  size_t size() const { return count; }

private:
  template <class ...Args>
  node_type *create_node(Args &&...args)
  {
    node_type *new_node = alloc.allocate(sizeof(*new_node));
    new (static_cast<void *>(new_node)) node_type(std::forward<Args>(args)...);
    return new_node;
  }

  iterator insert_after(const_iterator pos, node_type *new_node) noexcept
  {
    auto p = pos.get();
    new_node->next = p->next;
    p->next = new_node;
    count++;
    return iterator(new_node);
  }

  using internal_allocator_type =
    typename Allocator::template rebind<node_type>::other;

  internal_allocator_type alloc;
  detail::node_base root;
  size_t count;
};


template<typename T, typename U>
static inline
void swap(forward_list<T, U> &l1, forward_list<T, U> &l2) noexcept
{ return l1.swap(l2); }


} //namespace ll


#endif
