#ifndef BST_H
#define BST_H

#ifdef __cplusplus
extern "C" {
#endif

struct node
{
  int value;
  struct node *left;
  struct node *right;
};

struct node *bst_create_node(int val);
void bst_destroy_node(struct node *n);
struct node **bst_find(struct node **start, int value);
struct node **bst_find_min(struct node **start);

void bst_swap(struct node *n1, struct node *n2);
struct node *bst_insert(struct node *n, int value);
struct node *bst_remove(struct node *n, int value);
struct node *bst_balance(struct node *n);
struct node *bst_search(struct node *n, int value);
struct node *bst_minimum(struct node *n);
unsigned bst_depth(struct node *n);
void bst_destroy_all(struct node *n);

void bst_visit(struct  node *n, void *p, void (*f)(struct node *, void *));
void bst_visit_pre(struct  node *n, void *p, void (*f)(struct node *, void *));
void bst_visit_post(struct  node *n, void *p, void (*f)(struct node *, void *));

#ifdef __cplusplus
}
#endif

#endif
