#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include "bst.h"

struct node *bst_create_node(int val)
{
    struct node *n = malloc(sizeof *n);
    n->value = val;
    n->left = 0;
    n->right = 0;
    return n;
}

void bst_destroy_node(struct node *n)
{
    free(n);
}

struct node **bst_find(struct node **start, int value)
{
    struct node **next = start;

    for (struct node *n = *next; n; n = *next) {
        if (n->value == value)
            return next;

        next = (value < n->value) ? &n->left : &n->right;
    }

    return next;
}

struct node **bst_find_min(struct node **start)
{
    struct node **next = start;
    for (struct node *n = *next; n && n->left; n = *next)
        next = &n->left;

    return next;
}

void bst_swap(struct node *n1, struct node *n2)
{
    int tmp = n1->value;
    n1->value = n2->value;
    n2->value = tmp;
}

struct node *bst_insert(struct node *n, int value)
{
    struct node **p = bst_find(&n, value);
    if (*p) {
        printf("duplicate found\n");
        return *p;
    }

    return *p = bst_create_node(value);
}

// algorithm from
// http://www.algolist.net/Data_structures/Binary_search_tree/Removal
struct node *bst_remove(struct node *n, int value)
{
    struct node **p = bst_find(&n, value);
    if (!*p)
        return *p;

    struct node *to_delete = *p;
    if (to_delete->left == NULL) {
        *p = to_delete->right;
    }
    else if (to_delete->right == NULL) {
        *p = to_delete->left;
    }
    else {
        struct node **min_right = bst_find_min(&to_delete->right);
        bst_swap(to_delete, *min_right);
        to_delete = *min_right;
        *min_right = NULL;
    }

    return to_delete;
}

static
unsigned tree_to_vine(struct node **root)
{
    struct node **pp = root;
    struct node *n;
    unsigned count = 0;

    while ((n = *pp) != NULL) {
        if (n->right == NULL) {
            pp = &n->left;
            count++;
        }
        else {
            struct node *r = n->right;

            n->right = r->left;
            r->left = n;
            *pp = r;
        }
    }

    return count;
}

static
void compress(struct node *root, unsigned count)
{
    struct node **pp = &root;

    for (unsigned i=0; i<count; i++) {
        struct node *p = (*pp)->left;

        (*pp)->left = p->left;
        pp = &((*pp)->left);
        p->left = (*pp)->right;
        (*pp)->right = p;
    }
}

static inline
unsigned nearest_power_of_two(unsigned size)
{
    while (1) {
        unsigned next = size & (size - 1);
        if (next == 0)
            break;
        size = next;
    }

    return size;
}

static
void vine_to_tree(struct node *root, unsigned size)
{
    unsigned leaves = size + 1 - nearest_power_of_two(size + 1);

    compress(root, leaves);
    size -= leaves;
    while (size > 1) {
        compress(root, size / 2);
        size = size/ 2;
    }
}

// algorithm from wikipedia
// https://en.wikipedia.org/wiki/Day%E2%80%93Stout%E2%80%93Warren_algorithm
// http://web.eecs.umich.edu/~qstout/pap/CACM86.pdf
struct node *bst_balance(struct node *n)
{
    struct node pseudo_root = {-1, 0, 0};

    unsigned size = tree_to_vine(&n);
    pseudo_root.left = n;
    vine_to_tree(&pseudo_root, size);

    return pseudo_root.left;
}

struct node *bst_search(struct node *n, int value)
{
    return *bst_find(&n, value);
}

struct node *bst_minimum(struct node *n)
{
    return *bst_find_min(&n);
}

static
unsigned bst_depth_counter(struct node *n, unsigned depth, unsigned current_max)
{
    if (!n) {
        return depth > current_max ? depth : current_max;
    }

    unsigned left_max = bst_depth_counter(n->left, depth + 1, current_max);
    unsigned right_max = bst_depth_counter(n->right, depth + 1, current_max);
    unsigned local_max = left_max > right_max ? left_max : right_max;

    return local_max > current_max ? local_max : current_max;
}

unsigned bst_depth(struct node *n)
{
    return bst_depth_counter(n, 0, 0);
}

void bst_destroy_all(struct node *n)
{
    if (!n)
        return;

    struct node *left = n->left;
    struct node *right = n->right;
    bst_destroy_all(left);
    bst_destroy_all(right);
    bst_destroy_node(n);
}

void bst_visit(struct node *n, void *p, void (*f)(struct node *, void *))
{
    if (!n)
        return;

    bst_visit(n->left, p, f);
    f(n, p);
    bst_visit(n->right, p, f);
}

void bst_visit_pre(struct node *n, void *p, void (*f)(struct node *, void *))
{
    if (!n)
        return;

    f(n, p);
    bst_visit_pre(n->left, p, f);
    bst_visit_pre(n->right, p, f);
}

void bst_visit_post(struct node *n, void *p, void (*f)(struct node *, void *))
{
    if (!n)
        return;

    bst_visit_post(n->left, p, f);
    bst_visit_post(n->right, p, f);
    f(n, p);
}

