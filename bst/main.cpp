#include <iostream>
#include <cstdlib>

#include "bst.h"

template <typename T>
const void *addr(const T * const&p)
{
  return static_cast<const void *>(p);
}

std::ostream &operator<<(std::ostream &out, const node *n)
{
  out << "n (" << addr(n) << "): [" << n->value  << ", "
      << addr(n->left) << ", " << addr(n->right) << "]";
  return out;
}

node *create_tree()
{
  auto n = bst_insert(nullptr, 5);
  std::cout << n << "\n\n";

  auto n1 = bst_insert(n, 4);
  std::cout << " n: " << n << "\n";
  std::cout << "n1: " << n1 << "\n\n";

  auto n2 = bst_insert(n, 6);
  std::cout << " n: " << n << "\n";
  std::cout << "n1: " << n1 << "\n";
  std::cout << "n2: " << n2 << "\n\n";

  auto n3 = bst_insert(n, 3);
  std::cout << " n: " << n << "\n";
  std::cout << "n1: " << n1 << "\n";
  std::cout << "n2: " << n2 << "\n";
  std::cout << "n3: " << n3 << "\n\n";

  auto n4 = bst_insert(n, 4);
  std::cout << " n: " << n << "\n";
  std::cout << "n1: " << n1 << "\n";
  std::cout << "n2: " << n2 << "\n";
  std::cout << "n3: " << n3 << "\n";
  std::cout << "n4: " << n4 << "\n\n";

  return n;
}

void print_tree(struct node *n)
{
  bst_visit_pre(n, nullptr, [](struct node *n, void *) {
    printf("[%d: %d %d]\n",  n->value, n->left ? n->left->value : -1,
           n->right ? n->right->value : -1);
  });
}

void print_min(struct node *n)
{
  auto m = bst_minimum(n);
  printf("min: %d\n", m->value);
}


void check_search(node *n)
{
  printf("should have 3, 4, 5, 6\n");
  for (int i=0; i<10; i++) {
    node *p = bst_search(n, i);
    printf("%d.", i);
    if (p) {
      printf("found: %p\n", (void *)p);
    }
    else {
      printf("not found\n");
    }
  }
}

void check_visit(node *n)
{
  printf("inorder: ");
  bst_visit(n, nullptr, [](node *n, void *) {
    printf("%d ", n->value);
  });
  printf("\n");

  printf("preorder: ");
  bst_visit_pre(n, nullptr, [](node *n, void *) {
    printf("%d ", n->value);
  });
  printf("\n");

  printf("postorder: ");
  bst_visit_post(n, nullptr, [](node *n, void *) {
    printf("%d ", n->value);
  });
}

void check_remove()
{
  auto t = bst_insert(nullptr, 5);
  print_min(t);
  bst_insert(t, 3);
  print_min(t);
  bst_insert(t, 4);
  print_min(t);
  bst_insert(t, 1);
  print_min(t);
  bst_insert(t, 2);
  print_min(t);
  bst_insert(t, 0);
  print_min(t);
  bst_insert(t, 7);
  print_min(t);
  bst_insert(t, 6);
  print_min(t);
  bst_insert(t, 8);
  print_min(t);
  bst_insert(t, 9);
  print_min(t);
  printf("\n\n");
  print_tree(t);

  auto e1 = bst_remove(t, 6);
  printf("have: %d\n", e1->value);
  print_tree(t);

  auto e2 = bst_remove(t, 8);
  printf("have: %d\n", e2->value);
  print_tree(t);

  auto e3 = bst_remove(t, 3);
  printf("have: %d\n", e3->value);
  print_tree(t);

  bst_destroy_all(t);
  bst_destroy_node(e3);
  bst_destroy_node(e2);
  bst_destroy_node(e1);
}

void check_balance()
{
  printf("before balance\n");
  node *t = NULL;
  for (int i=0; i<10; i++)
    if (!t)
      t = bst_insert(t, i);
    else
      bst_insert(t, i);
  print_tree(t);
  printf("depth before: %u\n", bst_depth(t));
  printf("\n\n");

  t = bst_balance(t);
  printf("after balance\n");
  print_tree(t);
  printf("depth after: %u\n", bst_depth(t));

  bst_destroy_all(t);
}


int main()
{
  node *n = create_tree();
  printf("\n\n");

  check_search(n);
  check_visit(n);
  printf("\n\n");

  printf("add 0-9\n");
  for (int i=0; i<10; i++)
    bst_insert(n, i);
  check_visit(n);
  printf("\n\n");

  check_remove();
  printf("\n\n");

  check_balance();
  printf("\n\n");

  bst_destroy_all(n);
}
