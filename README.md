I picked a handful of projects laying around on my
hard drive.  I think these are a good sampling of
how I write programs.

bst:
    binary search tree in C.  no frills.
    the only thing to notice is that bst_find returns a pointer to the node
    you are looking for which simplifies the logic for all the other calls.


constexpr_props:
    c++11 and newer introduced a new tool to the programming tool belt.  constexpr.
    for our work at ripple i tried to make all our dispatch tables (enum -> command)
    into compile time constants using constexpr.   this is my scratch work in that
    direction

ll:
    c++ std::list replacement.  Tried to stay as true to the spec as possible.
    The allocator stuff isn't quite right yet, and it became no fun.


memmap_trick:
    a neat trick for making circular queues using memory mapping to double the
    buffer coverage.

