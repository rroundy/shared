#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <stdexcept>
#include <type_traits>

#include <unistd.h>
#include <linux/memfd.h>
#include <sys/syscall.h>
#include <sys/mman.h>
#include <sys/types.h>



struct queue
{
  int m_fd;
  void *m_buf;
  size_t m_size;

  queue();
  ~queue();
};

template <typename T>
struct exit_scope
{
  bool enabled;
  T exit_fn_;

  void disable() { enabled = false; }
  exit_scope(const T &fn) : enabled(true), exit_fn_(fn) {}
  exit_scope(T &&fn) : enabled(true), exit_fn_(std::move(fn)) {}
  ~exit_scope() { if (enabled) exit_fn_(); }
};

template <typename T>
exit_scope<typename std::decay<T>::type> make_scope_guard(T &&exit_fn)
{
  return exit_scope<typename std::decay<T>::type>(std::forward<T>(exit_fn));
}



queue::queue() : m_fd(-1), m_buf(nullptr), m_size(0)
{
  int pg_size = getpagesize();
  int fd = memfd_create("test_q", 0);
  if (fd < 0) {
    perror("memfd_create");
    throw std::runtime_error("can't create memfd");
  }

  auto fd_guard = make_scope_guard([&](){
    printf("executed\n");
    close(fd);});

  if (ftruncate(fd, pg_size)) {
    perror("ftruncate");
    throw std::runtime_error("ftruncate failed");
  }

  void *p = mmap(NULL, 2*pg_size, PROT_NONE, MAP_PRIVATE | MAP_ANONYMOUS,
                 -1, 0);
  if (p == MAP_FAILED) {
    perror("mmap");
    throw std::runtime_error("mmap failed");
  }

  auto p_guard = make_scope_guard([&](){
    printf("p_guard executed\n");
    munmap(p, 2*pg_size);
  });

  void *p1 = mmap(p, pg_size, PROT_READ | PROT_WRITE, MAP_SHARED | MAP_FIXED,
                  fd, 0);
  if (p1 == MAP_FAILED) {
    perror("mmap [p1]");
    throw std::runtime_error("mmap fialed");
  }

  auto p1_guard = make_scope_guard([&](){
    printf("p1_guard executed\n");
    munmap(p1, pg_size);
  });


  void *p2 = mmap((char *)p + pg_size, pg_size, PROT_READ | PROT_WRITE,
                  MAP_SHARED | MAP_FIXED, fd, 0);
  if (p2 == MAP_FAILED) {
    perror("mmap [p2]");
    throw std::runtime_error("mmap fialed");
  }

  auto p2_guard = make_scope_guard([&](){
    printf("p2_guard executed\n");
    munmap(p2, pg_size);
  });


  p_guard.disable();
  p1_guard.disable();
  p2_guard.disable();
  fd_guard.disable();

  m_fd = fd;
  m_buf = p;
  m_size = 2 * pg_size;
}

queue::~queue()
{
  if (m_buf) {
    munmap((char *)m_buf + m_size / 2, m_size / 2);
    munmap((char *)m_buf, m_size / 2);
    munmap((char *)m_buf, m_size);
  }

  if (m_fd > 0)
    close(m_fd);
}

void binprint(int indent, const char *buf, int len)
{
  int rows = (len + 15) / 16;
  for (int r = 0; r < rows; r++) {
    for (int i=0; i<16; i++) {
      printf("%.*s", indent, "");
      if (r * 16 + i < len) {
        printf("%02x ", (unsigned)buf[r * 16 + i]);
        if (i == 7)
          putchar(' ');
      }
      else {
        printf("   ");
      }
    }

    printf("%*.s", 5, "");

    for (int i=0; i<16; i++) {
      printf("%.*s", indent, "");
      if (r * 16 + i < len) {
        if (isprint(buf[r * 16 + i]))
          putchar(buf[r * 16 + i]);
        else
          putchar('.');

        if (i == 7)
          putchar(' ');
      }
    }

    putchar('\n');
  }
}

int real_main()
{
  queue q;
  char *buf = (char *)q.m_buf;
  size_t page_sz = getpagesize();
  printf("size: %zu\n", q.m_size);
  printf("page size: %zu\n", page_sz);

  auto n = sprintf((char *)q.m_buf, "this is the start");
  printf("n= %d\n", (int)n);

  for (int i=0; i<(int)n; i++){
    putchar(((char *)q.m_buf)[i]);
  }
  putchar('\n');

  for (int i=0; i<(int)q.m_size; i++) {
    buf[i] = (char)( (unsigned)i & 0xFF );
  }

  for (int i=0; i<(int)q.m_size; i++) {
    if (buf[i] != (char)((unsigned) i & 0xFF)) {
      printf("failed at %d got %02x expected %02x\n",
             i, (unsigned)buf[i],
             (unsigned)((char)((unsigned) i & 0xFF)));
      break;
    }
  }

  std::memset(buf, 0, page_sz);


  printf("before:\n");
  binprint(2, buf, 16);


  char *start = (char *)q.m_buf + (q.m_size/2 - 10);
  printf("buf: %p start: %p delta: %d\n", q.m_buf, (void *)start,
         (int)((char *)start - (char *)q.m_buf)
         );
  n = sprintf(start, "rob was here, so let's see where this goes.");
  printf("n= %d\n", (int)n);
  printf("after:\n");
  binprint(2, buf, 16);


  printf("all:\n");
  binprint(2, start, n);

  return EXIT_SUCCESS;
}


int main()
{
  try {
    return real_main();
  }
  catch(std::exception &e) {
    printf("caught exception: %s\n", e.what());
    return EXIT_FAILURE;
  }

  __builtin_unreachable();
}
