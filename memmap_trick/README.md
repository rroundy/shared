implementing a fast circular buffer using the trick of double mmapping
overlapping pages.

see
https://abhinavag.medium.com/a-fast-circular-ring-buffer-4d102ef4d4a3
https://lo.calho.st/posts/black-magic-buffer/
https://fgiesen.wordpress.com/2012/07/21/the-magic-ring-buffer/
