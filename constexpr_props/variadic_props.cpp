#include <cstdio>
#include <utility>
#include <array>
#include <tuple>
#include <type_traits>

using Action = void (*)(int);

void type_a(int);
void type_b(int);
void type_c(int);

struct prop_def
{
  int start;
  int count;
  Action action;
};

struct real_prop : prop_def
{
  int other_data;

  void do_something() {
    printf("start: %d count %d Action: %p other_data: %d\n",
           start, count, (void *)action, other_data);
  }

  constexpr real_prop(int start_, int count_, Action action_, int data_):
    prop_def{start_, count_, action_},
    other_data{data_}
  {}
};


struct dissimilar
{
  Action action;
  int c;
  int start;
  int count;
};

namespace detail {


namespace test {
struct x1 {
  size_t start;
};

struct x2 {
};

struct x3 {
  double start;
};

struct x4 {
  const char *start;
};

struct x5 {
  int start() { return 0; }
};

struct y1a { int start; };
struct y1b { int count; };
struct y1c { Action action; };

struct y2a { int start; int count;};
struct y2b { int start; Action action; };
struct y2c { int count; Action action; };

} //namespace test


template<typename ...>
using void_t = void;

template <typename T, typename = detail::void_t<>>
struct has_start : std::false_type{};

template <typename T>
struct has_start<T, detail::void_t<decltype(T::start)>> :
std::is_integral<decltype(T::start)>{};

static_assert(has_start<prop_def>::value == true, "");
static_assert(has_start<real_prop>::value == true, "");
static_assert(has_start<dissimilar>::value == true, "");
static_assert(has_start<test::x1>::value == true, "");
static_assert(has_start<test::x2>::value == false, "");
static_assert(has_start<test::x3>::value == false, "");
static_assert(has_start<test::x4>::value == false, "");
static_assert(has_start<test::x5>::value == false, "");

static_assert(has_start<test::y1a>::value == true, "");
static_assert(has_start<test::y1b>::value == false, "");
static_assert(has_start<test::y1c>::value == false, "");
static_assert(has_start<test::y2a>::value == true, "");
static_assert(has_start<test::y2b>::value == true, "");
static_assert(has_start<test::y2c>::value == false, "");


template <typename T, typename = detail::void_t<>>
struct has_count: std::false_type{};

template <typename T>
struct has_count<T, detail::void_t<decltype(T::count)>> :
std::is_integral<decltype(T::count)>{};

static_assert(has_count<prop_def>::value == true, "");
static_assert(has_count<real_prop>::value == true, "");
static_assert(has_count<dissimilar>::value == true, "");
static_assert(has_count<test::y1a>::value == false, "");
static_assert(has_count<test::y1b>::value == true, "");
static_assert(has_count<test::y1c>::value == false, "");
static_assert(has_count<test::y2a>::value == true, "");
static_assert(has_count<test::y2b>::value == false, "");
static_assert(has_count<test::y2c>::value == true, "");


template <typename T, typename = detail::void_t<>>
struct has_action: std::false_type{};

template <typename T>
struct has_action<T, detail::void_t<decltype(T::action(int{}))>> :
std::true_type{};

static_assert(has_action<prop_def>::value == true, "");
static_assert(has_action<real_prop>::value == true, "");
static_assert(has_action<dissimilar>::value == true, "");
static_assert(has_action<test::y1a>::value == false, "");
static_assert(has_action<test::y1b>::value == false, "");
static_assert(has_action<test::y1c>::value == true, "");
static_assert(has_action<test::y2a>::value == false, "");
static_assert(has_action<test::y2b>::value == true, "");
static_assert(has_action<test::y2c>::value == true, "");


namespace test {

} // namepspace test

template <typename T>
struct is_prop_def :
  std::conditional_t<has_start<T>::value,
    std::conditional_t<has_count<T>::value,
      std::conditional_t<has_action<T>::value, std::true_type, std::false_type>,
    std::false_type>,
  std::false_type>
{};



static_assert(is_prop_def<prop_def>::value == true, "");
static_assert(is_prop_def<real_prop>::value == true, "");
static_assert(is_prop_def<dissimilar>::value == true, "");

static_assert(is_prop_def<test::y1a>::value == false, "");
static_assert(is_prop_def<test::y1b>::value == false, "");
static_assert(is_prop_def<test::y1c>::value == false, "");

static_assert(is_prop_def<test::y2a>::value == false, "");
static_assert(is_prop_def<test::y2b>::value == false, "");
static_assert(is_prop_def<test::y2c>::value == false, "");


// https://stackoverflow.com/questions/39659127/restrict-variadic-template-arguments
template <bool ...B>
struct conjuction{};

template <bool Head, bool ...Tail>
struct conjuction<Head, Tail...> :
    std::integral_constant<bool, Head && conjuction<Tail...>::value>{};

template<bool B>
struct conjuction<B> : std::integral_constant<bool, B> {};
}


// each type must have
// .start .count  and .action

template <typename ...Types>
struct ttable
{
  using tuple_type = std::tuple<Types...>;
  tuple_type values;

  template <typename Tuple, size_t ...Idx>
  constexpr ttable(Tuple &&t, std::index_sequence<Idx...>):
    values { std::get<Idx>(std::forward<Tuple>(t))... }
  {}

  template <typename Tuple>
  constexpr ttable(Tuple &&t):
    ttable(std::forward<Tuple>(t),
           std::make_index_sequence<
           std::tuple_size<typename std::remove_reference_t<Tuple>>::value>())
  {}


  template <size_t ...Idx>
  constexpr size_t max_elt(std::index_sequence<Idx...>) const
  {
    size_t maxs[] = {
      static_cast<size_t>(
          std::get<Idx>(this->values).start \
          + std::get<Idx>(this->values).count)... };
    size_t M = maxs[0];
    for (size_t i=1; i<this->size(); i++) {
      size_t cur = maxs[i];
      if (cur > M)
        M = cur;
    }

    return M;
  }

  constexpr size_t max_elt() const
  {
    constexpr size_t sz = std::tuple_size<tuple_type>::value;
    return max_elt(std::make_index_sequence<sz>());
  }

  constexpr size_t size() const
  {
    return std::tuple_size<tuple_type>::value;
  }
};



template <size_t M>
struct dispatch_table
{
  Action handler[M] = {};

  constexpr int set_values(int n, int count, Action action)
  {
    for (int i=0; i<count; i++)
      handler[n + i] = action;
    return count;
  }

  template <typename ...Types, size_t ...Idx>
  constexpr dispatch_table(const ttable<Types...> &t,
                           std::index_sequence<Idx...>)
  {
    int ignore[] = {0,
      this->set_values(
          std::get<Idx>(t.values).start,
          std::get<Idx>(t.values).count,
          std::get<Idx>(t.values).action)... };
    (void)ignore;
  }

  template <typename ...Types>
  constexpr dispatch_table(const ttable<Types...> &t):
    dispatch_table(t, std::make_index_sequence<sizeof...(Types)>())
  {}

  constexpr size_t size() const { return M; }
};

template <typename ... Args,
         typename = std::enable_if_t<detail::conjuction<
          detail::is_prop_def<Args>::value...>::value>>
static constexpr auto make_ttable(Args &&...args)
{
  auto props = std::make_tuple(std::forward<Args>(args)...);
  return ttable<Args...>(props);
}


static constexpr auto st = make_ttable(
    prop_def{4, 5, type_a},
    real_prop{11, 4, type_b, 12},
    prop_def{20, 1, type_c},
    dissimilar{type_c, 7, 25, 5});

constexpr auto sz_st = sizeof(st);
constexpr auto st_reported_sz = st.size();
constexpr auto st_max = st.max_elt();

constexpr auto dt = dispatch_table<st_max>(st);
constexpr size_t sz_dt = sizeof(dt);


/*
namespace bad1 {
struct invalid {};
static constexpr auto st = make_ttable(
    prop_def{4, 5, type_a},
    real_prop{11, 4, type_b, 12},
    prop_def{20, 1, type_c},
    dissimilar{type_c, 7, 25, 5},
    invalid{});
}
*/


static_assert(dt.handler[ 0] == nullptr, "");
static_assert(dt.handler[ 1] == nullptr, "");
static_assert(dt.handler[ 2] == nullptr, "");
static_assert(dt.handler[ 3] == nullptr, "");
static_assert(dt.handler[ 4] == &type_a, "");
static_assert(dt.handler[ 5] == &type_a, "");
static_assert(dt.handler[ 6] == &type_a, "");
static_assert(dt.handler[ 7] == &type_a, "");
static_assert(dt.handler[ 8] == &type_a, "");
static_assert(dt.handler[ 9] == nullptr, "");
static_assert(dt.handler[10] == nullptr, "");
static_assert(dt.handler[11] == &type_b, "");
static_assert(dt.handler[12] == &type_b, "");
static_assert(dt.handler[13] == &type_b, "");
static_assert(dt.handler[14] == &type_b, "");
static_assert(dt.handler[15] == nullptr, "");
static_assert(dt.handler[16] == nullptr, "");
static_assert(dt.handler[17] == nullptr, "");
static_assert(dt.handler[18] == nullptr, "");
static_assert(dt.handler[19] == nullptr, "");
static_assert(dt.handler[20] == &type_c, "");
static_assert(dt.handler[21] == nullptr, "");
static_assert(dt.handler[22] == nullptr, "");
static_assert(dt.handler[23] == nullptr, "");
static_assert(dt.handler[24] == nullptr, "");
static_assert(dt.handler[25] == &type_c, "");
static_assert(dt.handler[26] == &type_c, "");
static_assert(dt.handler[27] == &type_c, "");
static_assert(dt.handler[28] == &type_c, "");
static_assert(dt.handler[29] == &type_c, "");
//static_assert(dt.handler[30] == &type_c, "");
static_assert(dt.size() == 30u, "");


int main()
{
  printf("st: %p sz: %zu cnt: %zu max_elt: %zu\n",
         (void *)&st, sz_st, st_reported_sz, st_max);

  printf(" dt: %p [%zu] cnt: %zu\n", (void *)&dt, sz_dt, dt.size());
}


void type_a(int x) { std::printf("a: %d\n", x); }
void type_b(int x) { std::printf("b: %d\n", x); }
void type_c(int x) { std::printf("c: %d\n", x); }
