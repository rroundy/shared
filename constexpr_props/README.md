one problem we have at Ripple is we have an enumeration of properties and have to dispatch
commands based on the property number that is sent in.  

This folder has some of my scratch work as i used the c++17 enhanced constexpr to 
build static dispatch tables at compile time.

This problem might be solved in a lower level language by writing a code generator.  Here
we are using modern c++ to be that code generator
