#include <cstdio>
#include <tuple>
#include <array>
#include <utility>
#include <initializer_list>



using Action = void (*)(int);
void type_a(int);
void type_b(int);
void type_c(int);

struct prop_def
{
  int start;
  int stop;
  Action action;
};


template <typename T, size_t N, size_t ...Idx>
constexpr static auto make_array_impl(const T (&arr)[N],
                                      std::index_sequence<Idx...>)
{
  using array_type = std::array<T, N>;
  return array_type { arr[Idx]... };

}

template <typename T, size_t N>
constexpr static auto make_array(const prop_def (&arr)[N])
{
  return make_array_impl<T>(arr, std::make_index_sequence<N>());
}



template <typename T, typename ...Args>
constexpr static auto make_array(Args &&...args)
{
  using array_type = std::array<T, sizeof...(Args)>;
  return array_type{ std::forward<Args>(args)... };
}

template <typename T, size_t N>
struct wrap
{
  T value[N];
};



template <typename T, size_t N, size_t ...Idx>
constexpr static auto make_c_array_impl(const T (&arr)[N],
                                      std::index_sequence<Idx...>)
{
  using array_type = wrap<T, N>;
  return array_type{ arr[Idx]... };

}

template <typename T, size_t N>
constexpr static auto make_c_array(const prop_def (&arr)[N])
{
  return make_c_array_impl<T>(arr, std::make_index_sequence<N>());
}


template <typename T, typename ...Args>
constexpr static auto make_c_array(Args &&...args)
{
  using array_type = wrap<T, sizeof...(Args)>;
  return array_type{ std::forward<Args>(args)... };
}




constexpr auto a1 = make_array<prop_def>(
    prop_def{4, 5, type_a},
    prop_def{11, 4, type_b},
    prop_def{20, 1, type_c});

constexpr auto a2 = make_array<prop_def>({
  {4, 5, type_a},
  {11, 4, type_b},
  {20, 1, type_c}
});


/*
 * no good
constexpr auto a3 = make_array<prop_def>(
    {4, 5, type_a},
    {11, 4, type_b},
    {20, 1, type_c}
    );
    */




struct dispatch_table
{
  template <size_t N>
  constexpr dispatch_table(const prop_def (&)[N]) {}
};

/*

template <typename ...Args>
constexpr auto make_dispatch_table(Args &&...args)
{
  constexpr auto small_table = make_array<prop_def>(std::forward<Args>(args)...);
  return dispatch_table(small_table);

}
*/

template <typename T, size_t N>
constexpr auto make_dispatch_table(const T (&arr)[N])
{
  constexpr auto small_table = make_array<T>(std::forward<T>(arr));
  return dispatch_table(small_table);
}



constexpr auto dt_a  = make_c_array<prop_def>({
  {4, 5, type_a},
  {11, 4, type_b},
  {20, 1, type_c}
}).value;

constexpr auto dt = make_dispatch_table(dt_a);


/*
template <typename T, size_t N>
constexpr static auto make_dt2(const T (&arr)[N])
{
  using array_type = std::array<typename std::remove_reference<T>::type, N);
  constexpr auto a = make_array_impl<T>(arr, std::make_index_sequence<N>());

  return make_dispatch_table(std::move(a));
}

constexpr auto dt2 = make_dt2<prop_def>({
  {4, 5, type_a},
  {11, 4, type_b},
  {20, 1, type_c}
});


*/
int main()
{
  printf("a1: %zu\n", sizeof(a1));
  printf("a2: %zu\n", sizeof(a2));
  printf("sizeof dt: %zu\n", sizeof(dt));
}

void generic_handler(char c, int x) { printf("%c: x[%d]\n", c, x); }
void type_a(int x) { generic_handler('a', x); }
void type_b(int x) { generic_handler('b', x); }
void type_c(int x) { generic_handler('c', x); }

