#include <cstdio>
#include <cstdlib>
#include <array>
#include <tuple>

typedef void (*Action)(int);
struct prop_def
{
  int start;
  int count;
  Action action;
};

void type_a(int);
void type_b(int);
void type_c(int);

constexpr static  prop_def small_table[] =
{
  {4, 5, type_a},
  {11, 4, type_b},
  {20, 1, type_c}
};

template <size_t N>
constexpr size_t max_size(const prop_def (&a)[N] )
{
  static_assert(N >= 1u, "empty array");
  size_t cur_max = a[0].start + a[0].count;
  for (size_t i=1; i<N; i++) {
    size_t this_max = a[i].start + a[i].count;
    if (this_max > cur_max)
      cur_max = this_max;
  }

  return cur_max;
}

constexpr Action small_table_lookup(int n)
{
  for (const auto &h : small_table) {
    if ((n >= h.start) && n < (h.start + h.count)) {
      return h.action;
    }
  }

  return nullptr;
}

template <size_t ... Idx>
constexpr auto get_pointers(std::index_sequence<Idx...>)
{
  constexpr size_t M = sizeof...(Idx);
  std::array<Action, M> actions = {
    small_table_lookup(Idx)...,
  };
  return actions;

}

constexpr auto make_big_array()
{
  constexpr size_t M = max_size(small_table);
  return get_pointers(std::make_index_sequence<M>());
}

static constexpr auto big_array = make_big_array();


static
void do_it(int n)
{
  Action action = small_table_lookup(n);
  if (action)  {
    action(n);
  }
  else {
    printf("unable to find handler for %d\n", n);
  }
}

int main()
{
  for (int i=0; i<25; i++) {
    do_it(i);
  }


  std::printf("big_array size: %zu\n", big_array.size());
  for (size_t i=0; i< big_array.size(); i++) {
    printf("%zu. %p\n", i, (void *)big_array[i]);
  }


}

void generic_handler(char c, int x) { printf("%c: x[%d]\n", c, x); }
void type_a(int x) { generic_handler('a', x); }
void type_b(int x) { generic_handler('b', x); }
void type_c(int x) { generic_handler('c', x); }


