#include <cstdio>
#include <utility>
#include <array>
#include <tuple>
#include <type_traits>

using Action = void (*)(int);

void type_a(int);
void type_b(int);
void type_c(int);

struct prop_def
{
  int start;
  int count;
  Action action;
};

struct real_prop : prop_def
{
  int other_data;

  void do_something() {
    printf("start: %d count %d Action: %p other_data: %d\n",
           start, count, (void *)action, other_data);
  }

  constexpr real_prop(int start_, int count_, Action action_, int data_):
    prop_def{start_, count_, action_},
    other_data{data_}
  {}
};

template <size_t N>
struct small_table
{
  prop_def values[N];

  constexpr small_table(const prop_def (&t)[N])  {
    for (size_t i=0; i<N; i++)
      values[i] = t[i];
  }

  template <typename Tuple, size_t ...Idx>
  constexpr small_table(Tuple &&t, std::index_sequence<Idx...>):
    values { std::get<Idx>(std::forward<Tuple>(t))... }
  {}

  template <typename Tuple>
  constexpr small_table(Tuple &&t):
    small_table(
        std::forward<Tuple>(t),
        std::make_index_sequence<
            std::tuple_size<typename std::remove_reference_t<Tuple>>::value>())
  {}

  constexpr size_t max_elt() const {
    size_t m = values[0].start + values[0].count;
    for (size_t i =1 ; i<N; i++) {
      size_t cur = values[i].start + values[i].count;
      if (cur > m)
        m = cur;
    }
    return m;
  }
};



template <size_t M>
struct dispatch_table
{
  Action handler[M] = {};


  template <size_t N>
  constexpr dispatch_table(const prop_def (&t)[N])
  {
    for (size_t i=0; i<N; i++) {
      for (int j=0; j <t[i].count; j++)
        handler[t[i].start + j] = t[i].action;
    }
  }

  template <size_t N>
  constexpr dispatch_table(const small_table<N> &t)
  {
    for (size_t i=0; i<N; i++) {
      for (int j=0; j <t.values[i].count; j++)
        handler[t.values[i].start + j] = t.values[i].action;
    }
  }

  constexpr size_t size() const { return M; }
};


struct s
{
  static constexpr prop_def small_table[] =
  {
    prop_def{4, 5, type_a},
    prop_def{11, 4, type_b},
    prop_def{20, 1, type_c}
  };
};



template <typename ... Args>
static constexpr auto make_small_table(Args &&...args)
{
  constexpr size_t sz = sizeof...(Args);
  auto props = std::make_tuple(std::forward<Args>(args)...);
  return small_table<sz>(props);
}

constexpr auto st = make_small_table(
    prop_def{4, 5, type_a},
    prop_def{11, 4, type_b},
    prop_def{20, 1, type_c});

constexpr size_t sz_st = sizeof(st);

constexpr auto st2 = make_small_table(
    prop_def{4, 5, type_a},
    real_prop{7, 2, type_b, 3});
constexpr size_t sz_st2 = sizeof(st2);

template <size_t N>
constexpr size_t max_elt(const prop_def (&t)[N])
{
  size_t m = t[0].start + t[0].count;
  for (size_t i =1 ; i<N; i++) {
    size_t cur = t[i].start + t[i].count;
    if (cur > m)
      m = cur;
  }
  return m;
}


template <typename T>
constexpr auto make_dispatch_table(const T&)
{
  constexpr size_t M = max_elt(T::small_table);
  return dispatch_table<M>{T::small_table};
}

constexpr auto dt = make_dispatch_table(s{});
constexpr size_t sz = sizeof(dt);

static_assert(dt.handler[0] == nullptr, "");
static_assert(dt.handler[1] == nullptr, "");
static_assert(dt.handler[2] == nullptr, "");
static_assert(dt.handler[3] == nullptr, "");
static_assert(dt.handler[4] == &type_a, "");
static_assert(dt.handler[5] == &type_a, "");
static_assert(dt.handler[6] == &type_a, "");
static_assert(dt.handler[7] == &type_a, "");
static_assert(dt.handler[8] == &type_a, "");
static_assert(dt.handler[9] == nullptr, "");
static_assert(dt.handler[10] == nullptr, "");
static_assert(dt.handler[11] == &type_b, "");
static_assert(dt.handler[12] == &type_b, "");
static_assert(dt.handler[13] == &type_b, "");
static_assert(dt.handler[14] == &type_b, "");
static_assert(dt.handler[15] == nullptr, "");
static_assert(dt.handler[16] == nullptr, "");
static_assert(dt.handler[17] == nullptr, "");
static_assert(dt.handler[18] == nullptr, "");
static_assert(dt.handler[19] == nullptr, "");
static_assert(dt.handler[20] == &type_c, "");
static_assert(dt.size() == 21u, "");
//static_assert(dt.handler[21] == nullptr, "");


constexpr auto dt2 = dispatch_table<st.max_elt()>(st);
constexpr size_t sz_dt2 = sizeof(dt2);

static_assert(dt2.handler[0] == nullptr, "");
static_assert(dt2.handler[1] == nullptr, "");
static_assert(dt2.handler[2] == nullptr, "");
static_assert(dt2.handler[3] == nullptr, "");
static_assert(dt2.handler[4] == &type_a, "");
static_assert(dt2.handler[5] == &type_a, "");
static_assert(dt2.handler[6] == &type_a, "");
static_assert(dt2.handler[7] == &type_a, "");
static_assert(dt2.handler[8] == &type_a, "");
static_assert(dt2.handler[9] == nullptr, "");
static_assert(dt2.handler[10] == nullptr, "");
static_assert(dt2.handler[11] == &type_b, "");
static_assert(dt2.handler[12] == &type_b, "");
static_assert(dt2.handler[13] == &type_b, "");
static_assert(dt2.handler[14] == &type_b, "");
static_assert(dt2.handler[15] == nullptr, "");
static_assert(dt2.handler[16] == nullptr, "");
static_assert(dt2.handler[17] == nullptr, "");
static_assert(dt2.handler[18] == nullptr, "");
static_assert(dt2.handler[19] == nullptr, "");
static_assert(dt2.handler[20] == &type_c, "");
static_assert(dt2.size() == 21u, "");
//static_assert(dt2.handler[21] == nullptr, "");


int main()
{
  printf(" dt: %p [%zu]\n",  (void *)&dt,     sz);
  printf(" st: %p [%zu]\n",  (void *)&st,  sz_st);
  printf("dt2: %p [%zu]\n", (void *)&dt2, sz_dt2);
  printf("st2: %p [%zu]\n", (void *)&st2, sz_st2);
}


void type_a(int x) { std::printf("a: %d\n", x); }
void type_b(int x) { std::printf("b: %d\n", x); }
void type_c(int x) { std::printf("c: %d\n", x); }
