#include <cstdio>
#include <initializer_list>
#include <unordered_map>
#include <utility>
#include <array>

typedef void (*action)(int);

template <int start, int stop, action act>
struct prop_range {};

struct range_holder
{
  int i;
  action act;
};

void print(int i);



template <typename T, T ...is>
constexpr auto make_array_from_seq_impl(std::integer_sequence<T, is...>,
                                        int start,
                                        action act)
{
  return std::array<range_holder, sizeof...(is)>{range_holder{start + is, act}...};
}


template <int start, int stop, action act>
constexpr auto make_array_from_prop_range(prop_range<start, stop, act>)
{
  return make_array_from_seq_impl(
      std::make_integer_sequence<int, stop - start>{}, start, act);
}

constexpr auto a = make_array_from_prop_range(prop_range<5, 10, print>{});
static_assert(a.size() == 5, "compile time");

///
//
//
//
//

template <typename T, T ...is>
constexpr auto make_map_from_seq_impl(std::integer_sequence<T, is...>,
                                        int start,
                                        action act)
{
  return std::unordered_map<int, range_holder>{
    {start+is, range_holder{start + is, act}}...};
}

template <int start, int stop, action act>
constexpr auto make_map_from_prop_range(prop_range<start, stop, act>)
{
  return make_map_from_seq_impl(
      std::make_integer_sequence<int, stop - start>{}, start, act);
}

/* constexpr */ const auto b = make_map_from_prop_range(prop_range<5, 10, print>{});
// static_assert(b.size() == 5, "compile time");


int main()
{
  printf("a.size: %zu\n", a.size());
  for (const auto &o: a) {
    o.act(o.i);
  }

  printf("\n");
  printf("b.size: %zu\n", b.size());
  for (int i=0; i<10; i++) {
    auto p = b.find(i);
    if (p != b.cend()) {
      printf("[found it %d]\n", i);
      const auto &elt = p->second;
      printf("[%d %p]: ", elt.i, (void *)elt.act);
      elt.act(elt.i);
    }
    else {
      printf("[not found %d]\n", i);
    }
  }
}


void print(int i)
{
  std::printf("%d\n", i);
}
