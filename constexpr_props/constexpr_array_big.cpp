#include <algorithm>
#include <cstdio>
#include <initializer_list>
#include <utility>
#include <array>
#include <numeric>
#include <tuple>

typedef void (*Action)(int);

void type_a(int);
void type_b(int);
void type_c(int);


struct prop {
  int start;
  int count;
  Action action;
};

struct handler {
  int val;
  Action  action;
};


template <typename Tuple>
auto tuple_helper(int n, Tuple t, const prop &p)
{
  if (n <= 0)
    return t;

  auto next_t = std::tuple_cat(
      t, std::make_tuple(p.start +  p.count - n, p.action));
  printf("last + (%d, %p)", p.start + p.count - n, (void *)p.action);
  return tuple_helper(n-1, next_t, p);
}

auto prop_2_tuple(const prop &p)
{
  printf("(%d, %p)", p.start, (void *)p.action);
  auto t = std::make_tuple(p.start, p.action);
  return tuple_helper(p.count - 1, t, p);
  printf("\n");
}


//constexpr auto t = prop_2_tuple(prop{4, 5, type_a});

//constexpr auto a = make_array(
//    prop{4, 5, type_a},
//    prop{11, 4, type_b>},
//    prop{20, 1, type_c});
//
//static_assert(a.size() == 10, "compile time");




int main()
{
  prop_2_tuple(prop{4, 5, type_a});
  //printf("%zu\n", sizeof(t));
//  printf("%d %p\n", get<0>(a), get<1>(a));
//  for (auto &h: a) {
//    std::printf("[%d %p]\n", h.val, (void *)h.action);
//  }

//  for (int j=0; j<30; j++)
//    do_it(j);
}


void generic_handler(char c, int x)
{
  printf("%c: x[%d]\n", c, x);
}

void type_a(int x) { generic_handler('a', x); }
void type_b(int x) { generic_handler('b', x); }
void type_c(int x) { generic_handler('c', x); }




