#include <cstdio>
#include <initializer_list>
#include <utility>
#include <array>
#include <numeric>
#include <tuple>

typedef void (*Action)(int);

void type_a(int);
void type_b(int);
void type_c(int);

template <int start, int count, Action action>
struct prop{};

struct handler {
  int val;
  Action  action;
};


namespace detail {

template <size_t ...is>
constexpr auto make_tuple_from_prop_impl(std::index_sequence<is...>,
                                         int start,
                                         Action action)
{
  return std::make_tuple(handler{static_cast<int>(is+start), action}...);
}


template <int start, int count, Action action>
constexpr auto make_tuple_from_prop(prop<start, count, action>)
{
  return detail::make_tuple_from_prop_impl(
      std::make_index_sequence<count>{}, start, action);
}


template<class Tuple, size_t ...Idx>
constexpr auto to_array(Tuple &&t, std::index_sequence<Idx...>)
{
  constexpr size_t sz = sizeof...(Idx);
  return std::array<handler, sz> {
    std::get<Idx>(t)...
  };
}


} // namespace detail


template <typename ...Options>
constexpr auto make_array(Options &&...options)
{
  auto t = std::tuple_cat(detail::make_tuple_from_prop(options)...);
  constexpr size_t sz = std::tuple_size<decltype(t)>::value;
  return detail::to_array(std::forward<decltype(t)>(t),
                          std::make_index_sequence<sz>{});
}


constexpr auto a = make_array(
    prop<4, 5, type_a>{},
    prop<11, 4, type_b>{},
    prop<20, 1, type_c>{});

template <size_t N>
constexpr size_t max_size(const prop_def (&a)[N])
{
}

static_assert(a.size() == 10, "compile time");
void do_it(int n)
{
  for (const auto &h : a) {
    if (h.val == n) {
      h.action(n);
      return;
    }
  }

  printf("unable to find handler for %d\n", n);
}

int main()
{
  for (auto &h: a) {
    std::printf("[%d %p]\n", h.val, (void *)h.action);
  }

  for (int j=0; j<30; j++)
    do_it(j);
}


void generic_handler(char c, int x) { printf("%c: x[%d]\n", c, x); }
void type_a(int x) { generic_handler('a', x); }
void type_b(int x) { generic_handler('b', x); }
void type_c(int x) { generic_handler('c', x); }




