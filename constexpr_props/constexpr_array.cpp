#include <cstdio>
#include <initializer_list>
#include <utility>
#include <array>

template <typename T, T ...is>
constexpr auto make_array_from_seq_impl(std::integer_sequence<T, is...>)
{
  return std::array<T, sizeof...(is)>{is...};
}


template <typename seq>
constexpr auto make_array_from_sequence(seq)
{
  return make_array_from_seq_impl(seq{});
}


constexpr auto a = make_array_from_sequence(std::make_integer_sequence<int, 10>{});
static_assert(a.size() == 10, "compile time");

int main()
{

  for (auto &i: a) {
    std::printf("%d\n", i);
  }

}
